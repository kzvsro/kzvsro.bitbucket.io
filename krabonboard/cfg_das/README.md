
# Úvod a přehled konfigurace

Aplikace při startu načte konfigurační soubor cfg_das.xml. Vyfiltrují se nepoužívané [komponenty] podle parametrů příkazové řádky. V cfg_das jsou definovány

 - [MeasurementAppSettings][]: Nastavení užitečná pro měřicí aplikaci jako takovou, netýkají se algoritmů - datové adresáře, omezení rychlosti, nastavení baterie atp.
 - [AutoCalibrationAppSettings][]: Nastavení užitečná pro autokalibrační aplikaci
 - [Devices][]: Konfigurace periferií (VGN, kamery atp.)
 - [Algorithm/Constants][]: Globální konstanty ovlivňující měření
 - [Algorithm/Domains][]: Jednotlivé domény a jejich kroky
 - [Algorithm/ImportantSignals][]: Popis signálů, se kterými se zachází speciálně
 - [Algorithm/Signals][]: Sestava měřicího řetězu (jaké signály se měří, zpracovávají a jejich algoritmy)
 - [Calibrations][]: Jednotlivé kalibrace s kalibračními konstantami pro jednotlivé kanály.
 
# Komponenty
 
Části cfg_das-u je možné vypínat a zapínat pomocí tzv. *komponent*. Na libovolném místě cfg_das-u je možné použít atribut `Components`. Při spuštění jádra je potom možné použít parametr `--disabled-components="..."`, a z cfg_dasu se při startu odfiltruje každý tag, který má všechny komponenty vyjmenované v parametrech z `--disabled-components`. 

Příklad:


*[cfg_das.xml](example-Components/cfg_das.xml)* (*[Parsed](example-Components/parsed-config.txt)*) | **Doc** *[Html](example-Components/index.html)*, *[Markdown](example-Components/index.md)* | **Processed data graphs** *[All domains](example-Components/processed-data-overall.svg)*, *[Time](example-Components/processed-data-time.svg)*, *[GPK](example-Components/processed-data-GPK.svg)*, *[GPS](example-Components/processed-data-GPS.svg)* | *[Processed data](example-Components/processed-data.txt)*

```xml
 <CFGDASvNET>
    <Devices>
        <Device Name="GPS01" Type="GPS" Components="GPS01">
            <Connections ActiveConnection="Simulation">
                <Connection Name="Simulation" Type="SimulationFunction"/>
            </Connections>
        </Device>
    </Devices>

    <Algorithm>
        <Domains ReferenceDomain="GPK">
            <Domain Name="GPS" SamplingSignal="ln" SamplingStep="0.25" Components="GPS01"/>
        </Domains>

        <ImportantSignals>
            <LatitudeSignalID Components="GPS01">Latitude</LatitudeSignalID>
            <LongitudeSignalID Components="GPS01">Longitude</LongitudeSignalID>
        </ImportantSignals>

        <Signals>
            <Signal Name="ln" Domain="GPK" Device="VGN01" Source="irc" Channel="0" IrcMode="Irc" Unit="km">
                <Description>Travelled distance</Description>
                <Algorithm Type="ResampleByThisSignal"/>
            </Signal>

            <Signal Name="Latitude" Domain="GPS" Device="GPS01" Source="Latitude" Channel="0" Archive="kzv" Unit="deg" Components="GPS01">
                <Description>GPS Latitude</Description>
            </Signal>
            <Signal Name="Longitude" Domain="GPS" Device="GPS01" Source="Longitude" Channel="0" Archive="kzv" Unit="deg" Components="GPS01">
                <Description>GPS Longitude</Description>
            </Signal>
            <Signal Name="Altitude" Domain="GPS" Device="GPS01" Source="Altitude" Channel="0" Archive="kzv" Unit="m" Components="GPS01">
                <Description>GPS Altitude</Description>
            </Signal>
            <Signal Name="FixQuality" Domain="GPS" Device="GPS01" Source="FixQuality" Channel="0" Unit="" Components="GPS01">
                <Description>Fix Quality (0 = No fix, > 0 = valid data)</Description>
            </Signal>
        </Signals>
    </Algorithm>
 </CFGDASvNET>
```

Pokud spustím jádro s parametrem `--disabled-components=GPS01`, všechny tagy z příkladu se vyfiltrují (zmizí), a GPS se vůbec nebude používat. Spouštění jádra typicky zařizuje Hub od Lukáše Bellady, podle příslušných přepínačů Hub vygeneruje seznam vypnutých komponent pro `--disabled-components` při spouštění jádra.

Tímto způsobem je snadno možné 

 - Udělat části cfg_das-u vypínatelné
 - anotovat jednotlivé části cfg_das-u podle příslušnosti.
 
*Je možné, aby nějaký tag příslušel k více komponentám. Příklad: `<Tag Components="C1,C2">`. Pro vypnutí takového tagu je třeba specifikovat **všechny** komponenty v `--disabled-components`. Tedy spuštění s `--disabled-componets="C1"` tag nevypne, ale spuštění s `--disabled-components="C1,C2"` už ano.*
 

# MeasurementAppSettings

Zde se nastavují věci, které nesouvisí přímo se způsobem vyhodnocování, spíše s během měřicí aplikace  jako takové. Do `header.xml` se neukládají.

Dobrý způsob, jak se rozhodnout, zda něco umístit do [MeasurementAppSettings][], nebo do [Algorithm/Constants][], je položit si následující otázku: "Potřebuji znát tuto hodnotu při recompute (vyhodnocení ze souboru)?" Pokud ano, jde nejspíše o konstantu ovlivňující algoritmus, a patří do [Algorithm/Constants][]. Pokud ne, nejspíše se týká čistě měřicí aplikace, a patří do [MeasurementAppSettings][].

```xml
<CFGDASvNET>
    <MeasurementAppSettings>
        <Constant Name="DataDirectory">./kms_data</Constant>
        <Constant Name="DeleteDataDirectoryAfterArchival">true</Constant>

        <!-- Povinné -->
        <Constant Name="WarnSpeed" Unit="km/h">30</Constant>
        <Constant Name="OverSpeed" Unit="km/h">35</Constant>

        <!-- Nepovinné -->
        <Constant Name="BatteryVoltageEmpty" Unit="V">12.0</Constant>
        <Constant Name="BatteryVoltageFull" Unit="V">16.8</Constant>
    </MeasurementAppSettings>
</CFGDASvNET>
```

## DataDirectory

Do kterého adresáře se ukládají odměřená data

## DeleteDataDirectoryAfterArchival

Měřená data se nejprve ukládají jako soubory a adresáře, a na konci měření se z nich teprve udělají archivy. Pokud je toto nastavení `true`, původní adresář s měřenými daty se po konci měření smaže, jinak ne.

## WarnSpeed, OverSpeed

Povinné. Pokud rychlost vozíku překročí `WarnSpeed`, uživatelské rozhraní by mělo zobrazit varování o příliš vysoké rychlosti. Při překročení `OverSpeed` jádro automaticky ukončí měření.

## BatteryVoltageEmpty, BatteryVoltageFull

Nepovinné. Pokud jsou uvedeny, značí napětí na baterii, která jsou považována za zcela vybitou a zcela nabitou baterii (tj. zobrazují se jako 0%, resp. 100% baterie).

Pokud je specifikováno, používá se na dvou místech:

 1. Pro indikaci stavu baterie v uživatelském rozhraní, vyčtená skutečná hodnota se porovnává se zde konfigurovaným rozsahem
 2. Při připojení k VGN kartě se pomocí povelu `SET_SETTINGS` nastaví, při jakém napětí má karta indikovat podpětí LEDkou (napětí < 15% rozsahu nad `BatteryVoltageEmpty`), a při jakém napětí má karta úpně vypnout (napětí < `BatteryVoltageEmpty`)

# AutoCalibrationAppSettings

Slouží pouze pro potřeby autokalibrační aplikace. 

Příklad:

```xml
<CFGDASvNET>
    <AutoCalibrationAppSettings>
        <Constant Name="NeedsTurning">false</Constant>
    </AutoCalibrationAppSettings>
</CFGDASvNET>
```

## NeedsTurning

Autokalibrace probíhá buď tak, že se jede do nějakého bodu, tam se otočí vozík, a jede se zpět do výchozího bodu (např. Krab), tato verze se konfiguruje pomocí `<Constant Name="NeedsTurning">true</Constant>`. Nebo se pouze jede ze startovního bodu do cílového, vozík se neotáčí (např. MMD), tato verze se konfiguruje pomocí `<Constant Name="NeedsTurning">false</Constant>`.

# Devices
 
TODO
 
# Algorithm/Constants
 
Zde jsou globální nastavení, které přímo souvisí s algoritmem měření a vyhodnocování. Viz také [MeasurementAppSettings][].

Příklad:

```xml
<CFGDASvNET>
    <Algorithm>
        <Constants>
            <Constant Name="TimeDomainBufferedSamples">100</Constant>
        </Constants>
    </Algorithm>
</CFGDASvNET>
```

##  TimeDomainBufferedSamples

Povinné, ale běžný uživatel asi nebude měnit. 

Kolik vzorků v časové doméně se má pamatovat před zahozením? V průběhu zpracování dat se mohou akumulovat další. Pokud by se jich naakumulovalo více než `TimeDomainBufferedSamples`, začala by se zahazovat a nebyla by zpracována. Příliš vysoké číslo zase znamená více zabrané paměti.
 
# Algorithm/Domains
 
Zde se konfigurují jednotlivé dráhové domény, tedy jejich názvy, vzorkovací kroky, a která doména se používá jako referenční v případech kdy není specifikována explicitně.

Příklad:


*[cfg_das.xml](example-Domains/cfg_das.xml)* (*[Parsed](example-Domains/parsed-config.txt)*) | **Doc** *[Html](example-Domains/index.html)*, *[Markdown](example-Domains/index.md)* | **Processed data graphs** *[All domains](example-Domains/processed-data-overall.svg)*, *[Time](example-Domains/processed-data-time.svg)*, *[GPK](example-Domains/processed-data-GPK.svg)* | *[Processed data](example-Domains/processed-data.txt)*

```xml
<CFGDASvNET>
    <Algorithm>
        <Domains ReferenceDomain="GPK">
            <Domain Name="TrackCamera" SamplingSignal="ln" SamplingStep="10"/>
            <Domain Name="RailProfiles" SamplingSignal="ln" SamplingStep="1.0"/>
            <Domain Name="GPK" SamplingSignal="ln" SamplingStep="0.25"/>
            <Domain Name="GPS" SamplingSignal="ln" SamplingStep="0.25"/>
            <Domain Name="ClearanceProfiles" SamplingSignal="ln" SamplingStep="0.04"/>
            <Domain Name="Corrugation" SamplingSignal="ln" SamplingStep="0.005"/>
        </Domains>
        <Signals>
            <Signal Name="ln" Domain="GPK" Device="VGN01" Source="irc" Channel="0" IrcMode="Irc" Unit="km">
                <Description>Travelled distance</Description>
                <Algorithm Type="ResampleByThisSignal"/>
            </Signal>
        </Signals>
    </Algorithm>
</CFGDASvNET>
```

Vidíme šest různých dráhových domén se standardizovanými jmény (seřazené od nejdelšího kroku)
 
 - `TrackCamera` Snímkové kamery
 - `RailProfiles` Profily kolejnic
 - `GPK` Geometrická poloha koleje - PK, VK, SK, RK, ZK atp.
 - `GPS` GPS souřadnice. Mají vlastní doménu, aby mohly fungovat i v případě absence domény GPK/Corrugation. 
 - `ClearanceProfiles` Průjezdné profily měřené pomocí LMS, přístroj LPS
 - `Corrugation` Vlnkovitost - Gekon
 
Pro každou doménu se uvádí vzorkovací krok v metrech, a název *vzorkovacího signálu* pro danou doménu. Dále se pomocí atributu `ReferenceDomain` vybírá tzv. *referenční doména*. 
 
## SamplingSignal vs ResampleByThisSignal

V cfg_das-u se nastavuje vzorkovací signál pomocí `SamplingSignal`, a vzorkovací kanál na Vgn pomocí `ResampleByThisSignal`. Jejich význam je podobný, ale ne shodný. Upřímně, mám pocit, že to ještě není perfektně vymyšlené, asi budou potřeba úpravy, pokud bude třeba např. vzorkovat podle posunutého signálu (jako to má MMD), signálu zprůměrovaného z více Irc (jako to má staré LPS), ale v tuto chvíli jsem nedokázal přijít s dokonalým řešením. Nicméně, nyní:

`SamplingSignal` se nastavuje pro **doménu**, zatímco `ResampleByThisSignal` se nastavuje pro **konkrétní Irc kanál na Vgn kartě**

`SamplingSignal` a `ReferenceDomain` se zohledňují na těchto místech:

 - Vzorkovací signál se používá jako vstupní signál pro [procesory, u kterých se neuvádí explicitní vstup](#synchronous-processor-01)
 - Autokalibrační aplikace vzorkuje podle vzorkovacího signálu v referenční doméně
 - `MeasurementStep` v header.xml v4 se bere z referenční domény. Ve verzi v5 se neuvádí.
 - `(End)SampleIndex` v events.xml v4 se počítá ve vzorcích referenční domény. Ve verzi v5 už se neuvádí.
 - `SampleIndex` .cam souborech v `camera.xml` se počítá ve vzorcích referenční domény. Ve verzi v5 už se neuvádí.
 
 `ResampleByThisSignal` se zohledňuje zde:

  - Aktuálně se všechny kanály Vgn karty převzorkovávají podle jednoho jejího Irc kanálu, a pomocí `ResampleByThisSignal` se určuje, který to má být, protože karta jich má víc. Není možné rozlišovat např. podle toho, že jde o `SamplingSignal` v dané doméně (např. ln), protože můžeme mít víc než jednu VGN kartu, ale nemůžeme mít víc než jeden signál `ln`, převzorkovací signál u druhé VGN karty se musí jmenovat jinak. Signál pro převzorkování tedy označujeme pomocí `<Algorithm Type="ResampleByThisSignal"/>`.
  - Měření umožňuje pozastavení (pauzu), během kterého se vlastně zahazují Irc vstupy, na které je přivedena dráha. To, které Irc vstupy se v pauze zahazují, se také nastavuje pomocí `ResampleByThisSignal`.
  
**Pozor**, do starých aplikací MMD a LPS jsou natvrdo nakódované výjimky:
 
  1. V MMD je vždy pauzovatelný Irc kanál signálu `pQMP` (i když `ResampleByThisSignal` by dávalo lepší smysl u signálu `QMP` - ale to není kanál). Autokalibrace v MMD používá jako vzorkovací signál `pQMP`
  2. V LPS jsou vždy pauzovatelné kanály `lnl` a `lnr`, v principu by nemělo být možné anotovat víc kanálů na jedné VGN kartě pomocí `ResampleByThisSignal`. Autokalibrace v LPS používá jako vzorkovací signál `lnr`
  
 
# Algorithm/ImportantSignals

V této sekci se uvádějí názvy signálů se speciálním významem, které mohou být jádru užitečné pro přídavnou funkcionalitu. Jsou nepovinné, pokud nejsou uvedeny, příslušná funkcionalita nebude dostupná. Jde o následující signály:

 - `SpeedSignalID` - Slouží k varování při vysoké rychlosti a ukončení měření při kritické rychlosti. Viz také [Konfigurace překročení rychlosti].
 - `ReleaseKrabSignalID` - Přes tento signál je možné nouzově odpojit kraba. Viz [Nouzové odpojení Kraba - ReleaseKrab]
 - `BatteryVoltageSignalID` - Slouží k vyčítání stavu baterie pro GUI.
 - `LatitudeSignalID`, `LongitudeSignalID` - Pokud jsou oba vyplněny, z příslušných signálů se bude vyčítat poloha, která se ukládá k traťovým událostem. Příslušné signály musí existovat v nějaké dráhové doméně, pokud existují ve více dráhových doménách, použije se ta s nejjemnějším vzorkovacím krokem.

# Algorithm/Signals

V sekci `<Algorithm>` je kompletně specifikován měřicí řetěz. Data

 1. Vznikají (jsou odměřována) v jednotlivých perifericích (VGN, profilové kamery atp.) *v časové doméně `Time`*. Surová data z periferií nazýváme *kanály*. Každá periferie typicky poskytuje různé kanály identifikované trojicí `Device:Source:Channel`, příklad: `VGN01:ad:8`
  2. Jsou převzorkována pomocí *resampleru* do některé dráhové domény. Zároveň jsou, tak jak jsou, posílána do domény `Time`, ta ovšem pojme max. [TimeDomainBufferedSamples](#TimeDomainBufferedSamples) vzorků, poté se nejstarší zahazují.
  3. Jsou zkalibrována z surových hodnot z AD převodníků apod. na fyzikální hodnoty pomocí kalibračních konstant. Od této chvíle se nenazývají *kanály*, ale *signály*. 
  4. *Signály* jsou dále zpracovávány pomocí *procesorů*, které z existujících signálů vypočítají nové. Nikdy nemění hodnoty v již existujícím signálu. *Procesor* má obecně N vstupních signálů a jeden výstupní.
  
## Odměřování kanálů a jejich převod na signály pomocí kalibrace

Body 1 - 3 z předchozí části se v cfg_das konfigurují např. takto: 


*[cfg_das.xml](example-Basic VGN signals definition/cfg_das.xml)* (*[Parsed](example-Basic VGN signals definition/parsed-config.txt)*) | **Doc** *[Html](example-Basic VGN signals definition/index.html)*, *[Markdown](example-Basic VGN signals definition/index.md)* | **Processed data graphs** *[All domains](example-Basic VGN signals definition/processed-data-overall.svg)*, *[Time](example-Basic VGN signals definition/processed-data-time.svg)*, *[GPK](example-Basic VGN signals definition/processed-data-GPK.svg)* | *[Processed data](example-Basic VGN signals definition/processed-data.txt)*

```xml
<CFGDASvNET>
    <Algorithm>
        <Signals>
            <Signal Name="ln" Domain="GPK" Device="VGN01" Source="irc" Channel="0" IrcMode="Irc" Unit="km">
                <Description>Travelled distance</Description>
                <Algorithm Type="ResampleByThisSignal"/>
            </Signal>
            <Signal Name="RK" Domain="GPK" Device="VGN01" Source="ad" Channel="1" Unit="mm">
                <Description>Primary Gauge</Description>
            </Signal>
            <Signal Name="Rych" Domain="GPK" Device="VGN01" Source="ad" Channel="16" Unit="km/h">
                <Description>Speed</Description>
            </Signal>
        </Signals>
    </Algorithm>
    <Calibrations>
        <Calibration Name="Kalibrace">
            <Channels>
                <Channel Device="VGN01" Source="irc" Channel="0" Mode="KQ">
                    <k>0.01</k>
                    <q>0</q>
                </Channel>
                <Channel Device="VGN01" Source="ad" Channel="1" Mode="KQ">
                    <k>0.01</k>
                    <q>10</q>
                </Channel>
                <Channel Device="VGN01" Source="ad" Channel="16" Mode="KQ">
                    <k>0.014124</k>
                    <q>0</q>
                </Channel>
            </Channels>
        </Calibration>
    </Calibrations>
</CFGDASvNET>
```

Vidíme definici tří kanálů/signálů `ln`, `RK`, `Rych` . Všechny jsou v doméně GPK, je u nich uvedeno, z jakého kanálu se berou. V sekci `Calibration` jsou potom uvedeny kalibrační konstanty K a Q pro jednotlivé kanály.

U signálu `ln` je navíc uvedeno `<Algorithm Type="ResampleByThisSignal"/>`. To je nápověda, že převzorkování pro zařízení VGN01 má probíhat právě podle tohoto signálu. Podrobnosti viz také kapitola [SamplingSignal vs ResampleByThisSignal]

## Počítané signály

V předchozím odstavci signály vznikaly přímo z kanálů jejich kalibrací. Signály mohou vznikat i výpočtem z jiných signálů pomocí výpočtu. Příklad:


*[cfg_das.xml](example-Computed signals definition/cfg_das.xml)* (*[Parsed](example-Computed signals definition/parsed-config.txt)*) | **Doc** *[Html](example-Computed signals definition/index.html)*, *[Markdown](example-Computed signals definition/index.md)* | **Processed data graphs** *[All domains](example-Computed signals definition/processed-data-overall.svg)*, *[Time](example-Computed signals definition/processed-data-time.svg)*, *[GPK](example-Computed signals definition/processed-data-GPK.svg)* | *[Processed data](example-Computed signals definition/processed-data.txt)*

```xml
<CFGDASvNET>
    <Algorithm>
        <Signals>
            <Signal Name="ln" Domain="GPK" Device="VGN01" Source="irc" Channel="0" IrcMode="Irc" Unit="km">
                <Description>Travelled distance</Description>
                <Algorithm Type="ResampleByThisSignal"/>
            </Signal>
            <Signal Name="qRK" Domain="GPK" Device="VGN01" Source="ad" Channel="1" Unit="mm">
                <Description>Quasi Gauge</Description>
            </Signal>
            <Signal Name="SK" Domain="GPK" Device="VGN01" Source="ad" Channel="2" Unit="mm">
                <Description>Alignment</Description>
            </Signal>
            <Signal Name="km" Domain="GPK" Unit="km">
                <Description>Kilometrage</Description>
                <Algorithm Type="Kilometrage">
                    <Input ID="Distance" Domain="GPK" Signal="ln"/>
                </Algorithm>
            </Signal>
            <Signal Name="RK" Domain="GPK" Unit="mm">
                <Description>Gauge</Description>
                <Algorithm Type="PrimaryGeometryContact" SubType="GaugeSLight">
                    <Input ID="QuasiGauge" Signal="qRK" Domain="GPK"/>
                    <Input ID="Alignment" Signal="SK" Domain="GPK"/>
                </Algorithm>
            </Signal>
        </Signals>
    </Algorithm>
</CFGDASvNET>
```

Vidíme zde tři primární signály `ln`, `qRK` a `SK`, a dva počítané signály `RK` a `km`. 

Primární signály mají vždy uvedené atributy `Device`, `Source`, `Channel`, pomocí kterých se identifikuje, odkud se berou jejich data.

Počítané signály mají vždy uvedenou sekci `Algorithm`, ve které se specifikuje, jakým způsobem se má počítaný signál počítat.

Vidíme, že se generuje signál `km` algoritmem `Kilometrage`. Algoritmus má jediný vstup `Distance`, na který přivádíme konkrétní signál `ln`, tedy primární signál ujeté dráhy. Algoritmus `Kilometrage` na základě startovní km, ujeté dráhy a událostí *"Změna kilometráže"* převádí vstupní signál na signál kilometráže.

Dále vidíme, že se generuje signál `RK` algoritmem `PrimaryGeometryContact-GaugeSLight`. Algoritmus očekává dva vstupy, kvazi-rozchod (na vstupu `QuasiGauge`) a směr. Na tyto vstupy navážeme konkrétní vstupní signály `qRK` a `SK`. Algoritmus pak počítá výsledný signál `RK` podle naprogramovaného vzorečku `output = Input_QuasiGauge - Input_Alignment`, tedy konkrétně v našem případě `RK = qRK - SK`.



**Obecný tvar sekce `Signal/Algorithm` je následující:**

```xml
<Algorithm Type="Povinná kategorie algoritmů" SubType="Konkrétní algoritmus v kategorii">
  <Input ID="ID 1. vstupu algoritmu" Signal="Signál na 1. vstupu" Domain="Doména signálu na 1. vstupu"/>
  <Input ID="ID 2. vstupu algoritmu" Signal="Signál na 2. vstupu" Domain="Doména signálu na 2. vstupu"/>
    ...
  <Constant Name="Konstanta algoritmu 1" Units="m">1.0</Constant>
  <Constant Name="Konstanta algoritmu 2" Units="mm">42.0</Constant>
    ...
</Algorithm>
```

Každý algoritmus je identifikován dvojicí `Type/SubType`. U některých typů (kategorií) algoritmů se nemusí `SubType` uvádět, protože v kategorii je jediný algoritmus, to je případ např. algoritmu `Kilometrage`

Algoritmus dále může mít vstupní signály a konstanty, které jej ovlivňují. Některé algoritmy ovšem používají konstanty z kalibrace, popř. globální konstanty, je třeba si vždy přečíst dokumentaci k příslušnému algoritmu.

Seznam všech algoritmů a jejich dokumentace je na konci tohoto textu. Popis algoritmu se zobrazí i v grafech vygenerované dokumentace cfg_dasu při najetí myší nad příslušný procesor.

## Časová doména `Time` (a doména `TimeDistance`) {#time-domain}

V cfg_das-u je pro každý kanál a signál uvedeno, v jaké dráhové doméně má vznikat. Převod do příslušné domény je zařízen převzorkováním.

Z praktických důvodů ale mnoho (ovšem ne všechny) signálů vznikají ještě jednou, v časové doméně `Time`, a to aniž by to bylo explicitně specifikováno. K tomu ještě existuje ještě doména `TimeDistance`, která slouží pro potřeby převzorkování z domény `Time` do dráhových domén.

Všechna odměřená data se ukládají do domény `Time` tak, jak jsou, bez jakéhokoliv převzorkování. Protože by jich bylo hodně, ukládá se jich jen omezený počet, který se nastavuje konstantou [TimeDomainBufferedSamples]. Starší vzorky se zahazují.

Data z domény `Time` mají dvě praktická využití:

 - Uživatelské rozhraní často potřebuje zobrazovat okamžité hodnoty, např. i když se stojí. Význam vzorků v doméně `Time` je tedy také "okamžitá hodnota".
 - Hodí se nějak zpracovat data již v časové doméně a teprve výsledek převzorkovat do dráhy, např. aby se předešlo ztrátě přesnosti.

Signály v doméně `Time` se generují automaticky (implicitně) vždy, když

 - se přímo odměřují data z nějakého zařízení
 - jeden signál se transformuje v jiný pomocí procesoru, který přepočítává vstup na výstup 1:1, tedy např. pro každý AD kanál spočte jeho kalibrovanou hodnotu. Takové procesory se nazývají **Synchronní** (`Synchronous`). 
 - nevznikají, když je uvedený algoritmus např. filtr klouzavého průměru, FFT filtr, nebo jiným způsobem neumožňuje přímý okamžitý přepočet každého jednoho vstupního vzorku na výstupní. Takové filtry jsou navíc často konfigurovány počtem vzorků, a dávaly by tudíž výrazně jiné výsledky v časové a dráhové doméně. Pokud je takový procesor skutečně vyžadován v časové doméně, je třeba jej nakonfigurovat explicitně. Tyto procesory se nazývají **Asynchronní**(`Asynchronous`).

Ukázkový cfg_das pro případ, kdy chceme počítat ojetí kolejnice v dráhové doméně z profilů v časové doméně, kvůli zvýšení přesnosti:


*[cfg_das.xml](example-TimeToDistance/cfg_das.xml)* (*[Parsed](example-TimeToDistance/parsed-config.txt)*) | **Doc** *[Html](example-TimeToDistance/index.html)*, *[Markdown](example-TimeToDistance/index.md)* | **Processed data graphs** *[All domains](example-TimeToDistance/processed-data-overall.svg)*, *[Time](example-TimeToDistance/processed-data-time.svg)*, *[GPK](example-TimeToDistance/processed-data-GPK.svg)*, *[RailProfiles](example-TimeToDistance/processed-data-RailProfiles.svg)* | *[Processed data](example-TimeToDistance/processed-data.txt)*

```xml
<CFGDASvNET>
    <Algorithm>
        <Signals>
            <Signal Name="ln" Domain="GPK" Device="VGN01" Source="irc" Channel="0" IrcMode="Irc" Unit="km">
                <Description>Travelled distance</Description>
                <Algorithm Type="ResampleByThisSignal"/>
            </Signal>

            <Signal Name="RailProfileLeftInside" Domain="RailProfiles" Device="WeCat3dLeftInside" Source="Points" Channel="0" MaxBufferedSamples="1000" Archive="krx" Unit="mm">
                <Description>Profily leva vnitrni kolejnice</Description>
            </Signal>

            <Signal Name="ProcessedRailProfileLeftInside" Domain="RailProfiles" MaxBufferedSamples="1000" Unit="mm">
                <Description>Zpracovane profily leva vnitrni kolejnice</Description>
                <Algorithm Type="RailProfile" SubType="Process">
                    <Input ID="CalibratedProfile" Domain="RailProfiles" Signal="RailProfileLeftInside"/>
                </Algorithm>
            </Signal>

            <Signal Name="HorizontalWearLeft" Domain="RailProfiles" Unit="mm">
                <Description>Vodorovné ojetí levé</Description>
                <Algorithm Type="RailProfile" SubType="HorizontalWear">
                    <Input ID="ProcessedProfile" Domain="Time" Signal="ProcessedRailProfileLeftInside"/>
                </Algorithm>
            </Signal>
        </Signals>
    </Algorithm>
</CFGDASvNET>
```

Z kamery `WeCat3dLeftInside` se odměřují profily do kanálu `RailProfileLeftInside@RailProfiles`. Automaticky se ovšem generují také do kanálu `RailProfileLeftInside@Time`. 

Profily `RailProfileLeftInside@RailProfiles` jsou dále zpracovány procesorem [RailProfile-Process], vzniká signál `ProcessedRailProfileLeftInside@RailProfiles`, ale automaticky se také generuje signál `ProcessedRailProfileLeftInside@Time` z `RailProfileLeftInside@Time`.

Nakonec ze zpracovaných signálů vybereme ojetí `HorizontalWearLeft`. Klíčové je, že vstupní signál je `ProcessedRailProfileLeftInside@Time` (viz řádek `<Input ID="ProcessedProfile" Domain="Time" Signal="ProcessedRailProfileLeftInside"/>`), nikoliv `ProcessedRailProfileLeftInside@Profiles` (což by se specifikovalo `<Input ID="ProcessedProfile" Domain="Profiles" Signal="ProcessedRailProfileLeftInside"/>`)

Díky tomu, že je jako vstupní signál uveden `ProcessedRailProfileLeftInside@Time`, se

 - přímo z něj generuje 1:1 signál `HorizontalWearLeft@Time`
 - z `HorizontalWearLeft@Time` se převzorkováním do dráhy pomocí `TimeDomainResampler`u generuje signál `HorizontalWearLeft@Profiles`.
 
 Kdyby byl jako vstupní signál uveden `ProcessedRailProfileLeftInside@Profiles`, výsledek by vypadal zdánlivě stejně, také by vznikly signály `HorizontalWearLeft@Time` a `HorizontalWearLeft@Profiles`. Postup jejich výpočtu by ale byl jiný:
 
  - `HorizontalWearLeft@Profiles` by vznikal 1:1 z `ProcessedRailProfileLeftInside@Profiles`
  - `HorizontalWearLeft@Time` by vznikal 1:1 z `ProcessedRailProfileLeftInside@Time` 

Protože převzorkování kolejnicových profilů vlastně znamená vybrat na celém dráhovém vzorku jediný profil a ostatní zahodit, ojetí by pro jeden dráhový vzorek bylo počítáno z jediného profilu. Když ho ale počítáme z časových vzorků, dostaneme ojetí z každého odměřeného profilu, a při převzorkování do dráhy se pak spočte jejich průměr pro daný dráhový vzorek.

## DO signály VGN. Ovládání brzdy (SwitchBreak). Procesory bez vstupního signálu - [UserCommand-SetDigSignal], [UserCommand-ReleaseKrab]. {#synchronous-processor-01}

Některé procesory v zásadě nepotřebují vstupní signál, počítají svůj výstup jiným způsobem. Jde hlavně o procesory, které přijímají povely od uživatelského rozhraní, procesory typu `UserCommand`. Například [UserCommand-SetDigSignal] je procesor, který čeká na povel od uživatelského rozhraní k nastavení nějakého digitálního výstupu např. na VGN kartě, používá se třeba k ovládání výhybkové brzdy Kraba.

            

*[cfg_das.xml](example-SwitchBreak/cfg_das.xml)* (*[Parsed](example-SwitchBreak/parsed-config.txt)*) | **Doc** *[Html](example-SwitchBreak/index.html)*, *[Markdown](example-SwitchBreak/index.md)* | **Processed data graphs** *[All domains](example-SwitchBreak/processed-data-overall.svg)*, *[Time](example-SwitchBreak/processed-data-time.svg)*, *[GPK](example-SwitchBreak/processed-data-GPK.svg)* | *[Processed data](example-SwitchBreak/processed-data.txt)*

```xml
<CFGDASvNET>
    <Algorithm>
        <Signals>
            <Signal Name="ln" Domain="GPK" Device="VGN01" Source="irc" Channel="0" IrcMode="Irc" Unit="km">
                <Description>Travelled distance</Description>
                <Algorithm Type="ResampleByThisSignal"/>
            </Signal>

            <Signal Name="SwitchBreakControl" Domain="GPK" Device="VGN01" Source="do" Channel="0" Unit="">
                <Description>Switch break control(0: Release break, 1: Activate break)</Description>
                <Algorithm Type="UserCommand" SubType="SetDigSignal"/>
            </Signal>
        </Signals>
    </Algorithm>
</CFGDASvNET>
```

*Poznámka k příkladu: na dráze 0.3 m simulovaný uživatel vložil povel pro nastavení signálu `SwitchBrakeControl` na `true`.*

Algoritmus [UserCommand-SetDigSignal] generuje signály `SwitchBreakControl@GPK` a `SwitchBreakControl@Time`. Na ně jsou přes procesory `DigitalOutputChannel` navázané příslušné výstupní kanály VGN `VGN01:do:0@GPK` a `VGN01:do:0@Time`. Kdykoliv se změní hodnota na posledním jmenovaném, `VGN01:do:0@Time`, vyšle se příslušný povel do VGN karty.

Z grafů procesorů je vidět, že procesor který generuje `SwitchBreakControl@GPK` má jeden vstup, na který je v našem případě navázaný signál `ln@GPK`. Ten se navázal automaticky, vybrán byl proto, že jde o vzorkovací signál domény `GPK`. Jeho konkrétní hodnoty se zahazují, ale pro každý vstupní vzorek je vygenerován jeden výstupní vzorek s hodnotou 1/0 pro DO VGN, vstupní signál tedy slouží jen jako podklad pro vzorkování výstupního signálu. Naproti tomu procesor který generuje `SwitchBreak@Time` nemá žádný vstup, novou hodnotu do signálu v časové doméně uloží pouze při přijetí povelu od uživatelského rozhraní.

## Nouzové odpojení Kraba - ReleaseKrab

Některé Kraby tahané za vozem je možné v kritické situaci nouzově odpojit pomocí vyslání hodnoty na některý DO VGN karty. To iniciuje uživatel z uživatelského rozhraní. 

Konfiguruje se následovně:


*[cfg_das.xml](example-ReleaseKrab/cfg_das.xml)* (*[Parsed](example-ReleaseKrab/parsed-config.txt)*) | **Doc** *[Html](example-ReleaseKrab/index.html)*, *[Markdown](example-ReleaseKrab/index.md)* | **Processed data graphs** *[All domains](example-ReleaseKrab/processed-data-overall.svg)*, *[Time](example-ReleaseKrab/processed-data-time.svg)*, *[GPK](example-ReleaseKrab/processed-data-GPK.svg)* | *[Processed data](example-ReleaseKrab/processed-data.txt)*

```xml
<CFGDASvNET>
    <Algorithm>
        <ImportantSignals>
            <ReleaseKrabSignalID>ReleaseKrab</ReleaseKrabSignalID>
        </ImportantSignals>
        <Signals>
            <Signal Name="ln" Domain="GPK" Device="VGN01" Source="irc" Channel="0" IrcMode="Irc" Unit="km">
                <Description>Travelled distance</Description>
                <Algorithm Type="ResampleByThisSignal"/>
            </Signal>

            <Signal Name="ReleaseKrab" Domain="GPK" Device="VGN01" Source="do" Channel="1" Units="">
                <Description>Emergency Krab release(0: Idle, 1: Release)</Description>
                <Algorithm Type="UserCommand" SubType="ReleaseKrab"/>
            </Signal>
        </Signals>
    </Algorithm>
</CFGDASvNET>
```

*Poznámka k příkladu: na dráze 0.3 m simulovaný uživatel vložil povel pro nouzové odpojení Kraba*

Definujeme si signál `ReleaseKrab`, který přijímá povel od uživatele pomocí procesoru [UserCommand-ReleaseKrab] (viz také předchozí kapitola). Tím je již zajištěno, že pokud uživatel vyšle povel k odpojení Kraba, jádro vyšle do VGN povel k zapsání `1` na příslušný DO, což by mělo zajistit odpojení Kraba.

Dále pomocí `ImportantSignals/ReleaseKrabSignalID` specifikujeme, že signál `ReleaseKrab` v časové doméně je ovládání odpojení Kraba. Jádro se k takovému signálu chová speciálně. Pokud zjistí, že se na něm vyskytla hodnota `1`, automaticky po jedné vteřině ukončí měření.

## Převzorkování signálu z jedné dráhové domény do jiné

Může se nám stát, že vstup algoritmu je v nějaké dráhové doméně, ale výstup potřebujeme v jiné.

Také se nám může stát, že máme již hotový signál v nějaké dráhové doméně, ale potřebovali bychom jej v jiné.

Oba případy se interně v KMS řeší stejně - za signál ve zdrojové doméně se pověsí procesor [DomainResampler], který převádí vstupní signál na výstupní signál se stejným jménem, ale jinou doménou. V praxi ale k této konfiguraci můžeme dojít dvěma způsoby.

### 1. Implicitní převzorkování 

V jednoduchých případech stačí uvést v `Algorithm` u vstupního signálu jinou doménu, než jaká je uvedená u samotného (výstupního) signálu. Procesor specifikovaný v `Algorithm` bude počítat výstupní signál ve stejné doméně jako vstupní signál, a za něj bude vložen [DomainResampler], který výstupní signál převzorkuje do požadované domény.

Příklad: Mám Gekona, který odměřuje ujetou dráhu v doméně `Corrugation`. Tuto doménu nemohu měnit, protože se podle ní zároveň převzorkovávají ostatní signály Gekona z té samé VGN (`qR0, qR1, ...`). Z dráhy se pomocí procesoru `Kilometrage` počítá signál `km` - kilometráž, kterou ale chci ukládat v doméně `GPK`, nikoliv `Corrugation`.

Použiji procesor [DomainResampler]:


*[cfg_das.xml](example-DomainResamplerImplicit/cfg_das.xml)* (*[Parsed](example-DomainResamplerImplicit/parsed-config.txt)*) | **Doc** *[Html](example-DomainResamplerImplicit/index.html)*, *[Markdown](example-DomainResamplerImplicit/index.md)* | **Processed data graphs** *[All domains](example-DomainResamplerImplicit/processed-data-overall.svg)*, *[Time](example-DomainResamplerImplicit/processed-data-time.svg)*, *[Corrugation](example-DomainResamplerImplicit/processed-data-Corrugation.svg)*, *[GPK](example-DomainResamplerImplicit/processed-data-GPK.svg)* | *[Processed data](example-DomainResamplerImplicit/processed-data.txt)*

```xml
<CFGDASvNET>
    <Algorithm>
        <Signals>
            <Signal Name="ln" Domain="Corrugation" Device="VGN01" Source="irc" Channel="0" IrcMode="Irc" Unit="km">
                <Description>Travelled distance</Description>
                <Algorithm Type="ResampleByThisSignal"/>
            </Signal>
            <Signal Name="qR0_Right" Domain="Corrugation" Device="VGN01" Source="ad" Channel="0" Unit="um">
                <Description>Corrugation sensor 0 Right</Description>
            </Signal>
            <Signal Name="km" Domain="GPK" Unit="km">
                <Description>Kilometrage</Description>
                <Algorithm Type="Kilometrage">
                    <Input ID="Distance" Domain="Corrugation" Signal="ln"/>
                </Algorithm>
            </Signal>
        </Signals>
    </Algorithm>
</CFGDASvNET>
```

Signál `ln` vzniká ve `VGN01` v doméně `Corrugation`. Procesor [Kilometrage] má uvedenou doménu vstupního signálu `Corrugation` a doménu výstupního signálu `GPK`. Proto se nejprve vytvoří procesor, který bude počítat `km@Corrugation` z `ln@Corrugation`, a za něj se vloží [DomainResampler], který `km@Corrugation` převzorkuje na `km@GPK`.

### 2. Explicitní převzorkování

[DomainResampler] je možné použít i explicitně pomocí 

```xml
<Signal Name="Output" Domain="OutputDomain" Unit="mm">
    <Description>Resampled signal</Description>
    <Algorithm Type="DomainResampler">
        <Input ID="Source" Signal="Input" Domain="Input distance domain or Time"/>
    </Algorithm>
</Signal>
```

## Prohazování levé a pravé kolejnice. Význam Left/Right, LeftRail/RightRail, (ne)prohazování kolejnic

**Poznámka: Příklad je aktuálně umělý, v praxi RailProfileLeftRailInside ani PlasticFlowLeftRail nepoužíváme. Příklad jsem psal v době, kdy jsem se domníval, že se prohazovat mají. I tak je celá dokumentace užitečná, např. v MMD se prohazování signálů děje (i když ta zatím neběží nad KMS, takže se tam žádné algoritmy v cfg_dasu nekonfigurují), jen je třeba si být při čtení dokumentace toho, že příklad je čistě ilustrační.**

### Co je v signálech uloženo

Do primárních signálů `PK`, `RK`, `SK`, `VK`, `ZK` `ZK2` se ukládají hodnoty *z pohledu vozíku*. To znamená, např. pro signál `PK` (převýšení koleje), že když je postavení vozíku opačné než směr kilometráže (`Kilometrage * Direction = -1`, jak by napsal Pepa Turek), skutečné převýšení vztažené ke koleji, které se pro vyhodnocení skutečně zajímavé, bude opačné než hodnota uložená do `Signal_PK.dat`. Tak je to zavedené, a musí se s tím poprat vyhodnocovací SW (Krab10).

Pokud se nějaký signál měří na obou stranách vozíku, bude se jmenovat

 - `Left`/`Right` pokud se ukládá z pohledu vozíku (levá/pravá ruka). Příklad: `Signal_RailProfileLeftInside.dat`
 - `LeftRail`/`RightRail` pokud se ukládá z pohledu kolejnice (levá/pravá kolejnice pokud se dívám ve směru stoupající km). Příklad: `Signal_SKDynLeftRail.dat` na MMD.

### Pojmenování `SK`/`VK` z levé/pravé kamery

Signály `SK`, `VK` se dají opticky odměřovat na levé kolejnici, nebo na pravé kolejnici (z pohledu vozíku). Při kontaktním měření se tradičně měřily na pravé kolejnici (z pohledu vozíku), a ukládály se jako `SK`, `VK` bez suffixu. Snažíme se toto zachovat ovšem s tím, že když je připojená pouze levá kamera, rádi bychom také měli `SK`, jen prostě z levé kamery.

**TODO** Není jasné, jak pojmenovávat hodnoty odměřené na levé kolejnici (z pohledu vozíku) - Pepa Turek navrhoval `SK_OtherSide`, což mi ale přijde dost ošklivé. 

**TODO** Také není jasné, jak se zachovat v případě, že je instalována pouze levá kamera (z pohledu vozíku), protože v takovém případě by se ukládalo pouze `SK_OtherSide`, což ale znamená, že chybí čisté `SK`, které Krab10 tradičně zpracovává.

### Jak zařídit prohazování signálů z pohledu vozíku (Left/Right) na pohled podle kolejnic (LeftRail/RightRail)

V cfg_dasu se prohazování zařídí tak, že nejprve vytváříme signály `Left`/`Right`, a nakonec použijeme algoritmus [SwapRails], který má dva vstupy, a v závislosti na směru km a postavení buď kopíruje na výstup signálu `LeftRail`/`RightRail` jeden nebo druhý vstup .

*Algoritmus [SwapRails] má to omezení, že neumožňuje prohazování signálů v časové doméně. Důvod je v podstatě implementační, ale významný - typické použití prohazuje data z různých zdrojů, např. levá a pravá Wenglor kamera. Data v časové doméně mají určenou svojí polohu (ujetou dráhu) příslušným signálem v doméně `TimeDistance`. Např. signál `RailProfileLeft@Time` má pro každý vzorek polohu určenou vzorkem se stejným indexem ze signálu `WeCat3dLeftInside@TimeDistance`, protože prvotně vzniká ze zařízení `WeCat3dLeftInside` (tento prvotní signál se nazývá **"Origin"**). Při průchodu dat procesory v časové doméně se nově vzniklým signálům přiřazuje příslušný polohový signál z `TimeDistance` podle vstupních signálů procesoru. Procesor [SwapRails], kdyby uměl prohazovat data v časové doméně, by vlastně měl nedeterministický polohový signál z `TimeDistance` podle toho, zda prohazuje nebo ne. Tomu se snažíme pro významné zjednodušení vyhnout, navíc nám to v praxi příliš nevadí.*)

Příklad prohazování v cfg_das-u 


*[cfg_das.xml](example-SwapRails/cfg_das.xml)* (*[Parsed](example-SwapRails/parsed-config.txt)*) | **Doc** *[Html](example-SwapRails/index.html)*, *[Markdown](example-SwapRails/index.md)* | **Processed data graphs** *[All domains](example-SwapRails/processed-data-overall.svg)*, *[Time](example-SwapRails/processed-data-time.svg)*, *[GPK](example-SwapRails/processed-data-GPK.svg)*, *[RailProfiles](example-SwapRails/processed-data-RailProfiles.svg)* | *[Processed data](example-SwapRails/processed-data.txt)*

```xml
<CFGDASvNET>
    <Algorithm>
        <Signals>
            <Signal Name="ln" Domain="GPK" Device="VGN01" Source="irc" Channel="0" IrcMode="Irc" Unit="km">
                <Description>Travelled distance</Description>
                <Algorithm Type="ResampleByThisSignal"/>
            </Signal>

            <Signal Name="RailProfileLeftInside" Domain="RailProfiles" Device="WeCat3dLeftInside" Source="Points" Channel="0" MaxBufferedSamples="1000" Unit="mm">
                <Description>Rail profiles left inside trolley side</Description>
            </Signal>
            <Signal Name="ProcessedRailProfileLeftInside" Domain="RailProfiles" MaxBufferedSamples="1000" Unit="mm">
                <Description>Processed rail profiles left inside trolley side</Description>
                <Algorithm Type="RailProfile" SubType="Process">
                    <Input ID="CalibratedProfile" Signal="RailProfileLeftInside" Domain="RailProfiles"/>
                </Algorithm>
            </Signal>
            <Signal Name="PlasticFlowLeft" Domain="GPK" Unit="mm">
                <Description>Plastic flow of rail on the left trolley side</Description>
                <Algorithm Type="RailProfile" SubType="PlasticFlow">
                    <Input ID="ProcessedProfile" Signal="ProcessedRailProfileLeftInside" Domain="Time"/>
                </Algorithm>
            </Signal>

            <Signal Name="RailProfileRightInside" Domain="RailProfiles" Device="WeCat3dRightInside" Source="Points" Channel="0" MaxBufferedSamples="1000" Unit="mm">
                <Description>Rail profiles right inside trolley side</Description>
            </Signal>
            <Signal Name="ProcessedRailProfileRightInside" Domain="RailProfiles" MaxBufferedSamples="1000" Unit="mm">
                <Description>Processed rail profiles right inside trolley side</Description>
                <Algorithm Type="RailProfile" SubType="Process">
                    <Input ID="CalibratedProfile" Signal="RailProfileRightInside" Domain="RailProfiles"/>
                </Algorithm>
            </Signal>
            <Signal Name="PlasticFlowRight" Domain="GPK" Unit="mm">
                <Description>Plastic flow of rail on the right trolley side</Description>
                <Algorithm Type="RailProfile" SubType="PlasticFlow">
                    <Input ID="ProcessedProfile" Signal="ProcessedRailProfileRightInside" Domain="Time"/>
                </Algorithm>
            </Signal>

            <Signal Name="RailProfileLeftRailInside" Domain="RailProfiles" MaxBufferedSamples="1000" Archive="kzv" Unit="mm">
                <Description>Rail profiles left inside rail</Description>
                <Algorithm Type="SwapRails">
                    <Input ID="+" Signal="RailProfileLeftInside" Domain="RailProfiles"/>
                    <Input ID="-" Signal="RailProfileRightInside" Domain="RailProfiles"/>
                </Algorithm>
            </Signal>
            <Signal Name="PlasticFlowLeftRail" Domain="GPK" Archive="kzv" Unit="mm">
                <Description>Plastic flow left inside rail</Description>
                <Algorithm Type="SwapRails">
                    <Input ID="+" Signal="PlasticFlowLeft" Domain="GPK"/>
                    <Input ID="-" Signal="PlasticFlowRight" Domain="GPK"/>
                </Algorithm>
            </Signal>

            <Signal Name="RailProfileRightRailInside" Domain="RailProfiles" MaxBufferedSamples="1000" Archive="kzv" Unit="mm">
                <Description>Rail profiles right inside rail</Description>
                <Algorithm Type="SwapRails">
                    <Input ID="+" Signal="RailProfileRightInside" Domain="RailProfiles"/>
                    <Input ID="-" Signal="RailProfileLeftInside" Domain="RailProfiles"/>
                </Algorithm>
            </Signal>
            <Signal Name="PlasticFlowRightRail" Domain="GPK" Archive="kzv" Unit="mm">
                <Description>Plastic flow right inside rail</Description>
                <Algorithm Type="SwapRails">
                    <Input ID="+" Signal="PlasticFlowRight" Domain="GPK"/>
                    <Input ID="-" Signal="PlasticFlowLeft" Domain="GPK"/>
                </Algorithm>
            </Signal>
        </Signals>
    </Algorithm>
</CFGDASvNET>
```

Nejprve si generujeme signály z pohledu vozíku, `Left`/`Right`. Ty generují data v dráhové i časové doméně. Na ukázku si generujeme vždy profil z kamery, ten poté zpracujeme, a ze zpracovaného profilu si vyzobneme převalek. Poté si pomocí [SwapRails] vybereme již v dráhové doméně příslušné `Left`/`Right` signály z pohledu vozíku do `LeftRail`/`RightRail` signálů z pohledu kolejnice, a tyto se teprve ukládají na disk.


### Volitelné vypínání (Komponenta) levého/pravého vstupu a SwapRails

Procesor [SwapRails] má ještě jednu užitečnou vlastnost. Není nutné, aby byly vyplněné oba vstupy, `+` a `-`. V případě,
že je vyplněný pouze jeden z nich, nebo dokonce žádný, bude výstupní signál vznikat pouze v případě, že postavení/směr km
odpovídají vyplněnému vstupu. Jinak výstupní signál vůbec nevzniká.

To je užitečné, pokud chceme udělat z jednotlivých vstupů odpojovatelné [Komponenty]. Příklad:

```xml
<Signals>
    <Signal Name="RailProfileLeftRailInside" Domain="RailProfiles" MaxBufferedSamples="1000" Archive="kzv" Unit="mm">
        <Description>Rail profiles left inside rail</Description>
        <Algorithm Type="SwapRails">
            <Input ID="+" Signal="RailProfileLeftInside" Domain="RailProfiles" Components="RailProfileLeftInside" />
            <Input ID="-" Signal="RailProfileRightInside" Domain="RailProfiles" Components="RailProfileRightInside" />
        </Algorithm>
    </Signal>
    <Signal Name="RailProfileRightRailInside" Domain="RailProfiles" MaxBufferedSamples="1000" Archive="kzv" Unit="mm">
        <Description>Rail profiles right inside rail</Description>
        <Algorithm Type="SwapRails">
            <Input ID="-" Signal="RailProfileLeftInside" Domain="RailProfiles" Components="RailProfileLeftInside" />
            <Input ID="+" Signal="RailProfileRightInside" Domain="RailProfiles" Components="RailProfileRightInside" />
        </Algorithm>
    </Signal>
</Signals>
```

*(pozn.: Shoda jmen v `Signal="RailProfileLeftInside"` a `Components="RailProfileLeftInside"` je čistě náhodná)*

V případě, že jsou všechny komponenty zapnuté, bude v `RailProfileLeftRailInside` buď profil z levé nebo pravé kolejnice podle toho, s jakou kombinací postavení/směr km se měření spustí. v `RailProfileRightRailInside` bude opačná kolejnice. Pokud ale bude zapnutá např. pouze komponenta `RailProfileLeftInside` a druhá kamera bude vypnutá, bude vznikat pouze signál `RailProfileLeftRailInside` (zabere první procesor) v případě že postavení vozíku = směr km, nebo pouze signál `RailProfileRightRailInside` (zabere druhý procesor) v případě, že postavení vozíku ≠ směr km. To zcela odpovídá realitě, kdy s jedinou kamerou je možné změřit pouze buď levou kolejnici nebo jen pravou kolejnici v závislosti na postavení vozíku vzhledem ke směru kilometráže.

**Na procesoru [SwapRails] s částečně definovanými vstupy nesmí záviset jiný procesor, protože by mohlo dojít k tomu, že signál na jeho vstupu nebude procesorem [SwapRails] generovaný. Je tedy třeba dát si obzvlášť pozor, protože cfg_das, který je korektní s `--disabled-components=""` může přestat být korektní např. při spuštění s `--disabled-components="RailProfileLeftInside`. Chyba bude nahlášena až při spuštění s problémovou kombinací `--disabled-components`!** Pokud by se ukázalo, že je to v praxi problém, šlo by naimplementovat to, že závislost by byla možná, a ani závislý procesor by nevznikal v případě špatné kombinace postavení/směr km.


## Konfigurace překročení rychlosti

V KMS je zabudované varování o překročení rychlosti a automatické ukončení měření při překročení rychlosti. 

Hodnocení rychlosti může nabývat tří hodnot: *OK*, *Varování - vysoká rychlost*, *Chyba - příliš vysoká rychlost a ukončení měření*. Jádro KMS periodicky posílá do GUI hodnocení rychlosti, GUI ji příslušně prezentuje uživateli. Když je rychlost příliš vysoká, jádro KMS měření automaticky ukončí.

Konfiguruje se následovně:


*[cfg_das.xml](example-SpeedWarning/cfg_das.xml)* (*[Parsed](example-SpeedWarning/parsed-config.txt)*) | **Doc** *[Html](example-SpeedWarning/index.html)*, *[Markdown](example-SpeedWarning/index.md)* | **Processed data graphs** *[All domains](example-SpeedWarning/processed-data-overall.svg)*, *[Time](example-SpeedWarning/processed-data-time.svg)*, *[GPK](example-SpeedWarning/processed-data-GPK.svg)* | *[Processed data](example-SpeedWarning/processed-data.txt)*

```xml
<CFGDASvNET>
    <MeasurementAppSettings>
        <Constant Name="WarnSpeed" Unit="km/h">30</Constant>
        <Constant Name="OverSpeed" Unit="km/h">35</Constant>
    </MeasurementAppSettings>
    <Algorithm>
        <ImportantSignals>
            <SpeedSignalID>Rych</SpeedSignalID>
        </ImportantSignals>
        <Signals>
            <Signal Name="ln" Domain="GPK" Device="VGN01" Source="irc" Channel="0" IrcMode="Irc" Unit="km">
                <Description>Travelled distance</Description>
                <Algorithm Type="ResampleByThisSignal"/>
            </Signal>
            <Signal Name="Rych" Domain="GPK" Device="VGN01" Source="ad" Channel="16" Unit="km/h">
                <Description>Speed</Description>
            </Signal>
        </Signals>
    </Algorithm>
</CFGDASvNET>
```

Konstantami `WarnSpeed` a `OverSpeed` se nastaví hranice pro varování a chybu příliš vysoké rychlosti.

V `ImportantSignals/SpeedSignalID` se vybere ID signálu, který reprezentuje rychlost vozíku (v praxi bude asi `Rych` nebo `RychSW`)

Nakonec musí příslušný signál rychlosti nějak vznikat, v našem případě nám dodává karta `VGN01` signál `Rych` na AD kanálu 16. 

**Poznámka**: Kalibrační konstanty pro rychlost z VGN a dráhu z Irc [jsou popsány zde](https://bitbucket.org/kzvsro/kzv-doc/wiki/KrabOnBoardIrcCalibration) 

## Konfigurace ukládání (atributy SaveMask, Archive)

U každého kanálu/signálu je možné zvolit, co a kam se má ukládat. 

### Archive - ukládání signálových souborů

Atribut `Archive` uvádí, do kterých archivů se má příslušný datový soubor uložit. Typická hodnota je `Archive="krx"` nebo `Archive="kzv"`, ale pro ladicí účely je klidně možné udělat třeba `Archive="kzv,dbg"`, soubor se pak uloží do archivu `.kzv` a .`dbg`. 

Hodnota `Archive="""` je totožné s úplným vynecháním atributu `Archive`.

To, zda se soubor vůbec ukládá, a s jakou přesností, je 

 - v CfgDas v4 pomocí atributu [SaveMask](#savemask)
 - v CfgDas v5 atribut `SaveMask` neexistuje. Soubor se prostě ukládá do těch archivů, do kterých byl zvolen pomocí atributu `Archive`. Kamerové obrázky se vždy ukládají ve formátu KŽV (jpeg snímky zabalené v archivu `.cam`). Datové soubory se vždy ukládají s jednoduchou přesností, jedinou výjimkou jsou signály `Latitude`, `Longitude`, `Altitude`, ty se vždy ukládají s dvojitou přesností.
 
Pro signál kamerových snímků z kanálů `CAMxx/Image/0` by se měl vždy použít `Archive="cam"`, zachází se s nimi speciálně.
 
### ChannelArchive (ArchiveChan)  - ukládání kanálových souborů

Atribut `ChannelArchive` (CfgDas v5), popř `ArchiveChan` (CfgDas v4) se chová úplně stejně jako atribut `Archive`, ale je možné ho použít pouze u kanálů. Specifikuje, do kterých archivů se mají ukládat soubory `Channel_domain_device_source_channel.dat`. V KMS se zatím v praxi nepoužívá, i když podle mě osobně by to mohlo být výhledově užitečné.


### SaveMask (CfgDas v4) {#savemask}

**Tato sekce se týká CfgDas v4, od verze 5 už se SaveMask vůbec nepoužívá!**

Specifikace ukládání je trochu nešťastné dědictví Liborova formátu.

Co konkrétně se má ukládat se nastavuje pomocí atributu `SaveMask`. Příklad: `SaveMask=0x0100`.

Prefix `0x` značí, že číslo je uvedené v hexadecimálním kódu. Čtyři šestnáctkové cifry definují dva bajty, horní bajt (`0x01`) specifikují způsob ukládání signálových dat, dolní bajt (`0x00`) kanálová data.

Původní [Liborův formát](https://bitbucket.org/kzvsro/dasv3-public/wiki/popis%20souboru%20cfg_das.xml.md) umožňoval specifikovat nejrozmanitější kombinace dat, přičemž ovšem ne všechny kombinace specifikace a typu signálu dávaly smysl. KMS z toho používá jen následující:

 - neukládat (0x00)
 - ukládat s jednoduchou přesností (0x01)
 - ukládat s dvojitou přesností (0x02)
 
Jednoduchá přesnost znamená ukládat floaty jako Float32, dvojitá jako Float64.

Speciálně se zachází s přesností u kamerových obrázků. Význam je následující:
 
 - `SaveMask 0x0100` znamená ukládáni ve formátu KŽV, tj. samostatný adresář s JPEG obrázky, camera.xml, zazipovaný do archivu .cam. V tomto případě není třeba uvádět atribut Archive="..."
 - `SaveMask 0x0200` ukládá ve formátu MEASProg, tj. samostatný soubor s binárními daty jeden obrázek za druhým. Je třeba generovat ještě druhý signál, `Length`, ve kterém jsou délky jednotlivých obrázků, aby bylo možné rekonstruovat původní data.


## TODO *Naimplementovat u vsech resampleru check, ze neni nikde specifikovan neplatny algoritmus (tj. jiny nez ResampleByThisSignal u Irc)*

# Calibrations

V této sekci jsou uvedeny kalibrační listy (kalibrace) jednotlivých kanálů. Různé druhy kanálů se kalibrují různě, jinak vypadá např. kalibrace AD snímače z VGN karty (kalibruje se pomocí konstant `K` a `Q`), jinak vypadá kalibrace profilové kamery (kalibruje se pomocí středu otáčení, úhlu, a několika příznaků kvůli levé/pravé koleji a pořadí bodů).

## Kalibrace KQ

Nejběžnější kalibrace slouží k převodu skalárního vstupu přivedeného na AD převodník na fyzikální veličinu. Kalibruje se pomocí dvou konstant, 

 - `K` - zisk, jakou hodnotu fyzikální veličiny má jeden bit převodníku
 - `Q` - posun, o kolik bitů je posunuta nula převodníku oproti nule fyzikální veličiny.
 
Platí: `Fyzikální veličina = K * (hodnota z převodníku - Q)`

**!!! __Převod se vždy vztahuje k veličině v základních jednotkách, tj. metry, kilogramy, radiány atp__ !!!**

Příklad (pozor, kalibrace má trochu jiný formát v cfg_das v4 a cfg_das v5, zde je už v5):

```xml
<Channel Device="VGN01" Source="ad" Channel="1" Mode="KQ">
    <k>0.000002</k>
    <q>20</q>
</Channel>
```

V některých případech (v praxi použito na MMD) může být potřeba mít trochu jinou konstantu `q` při jízdě ve správném a nesprávném postavení. V takovém případě je možné uvést rozdílné hodnoty `qUp` a `qDown`.  Příklad: 

```xml
<Channel Device="VGN01" Source="ad" Channel="1" Mode="KQ">
    <k>0.000002</k>
    <qUp>19</qUp>
    <qDown>21</qDown>
</Channel>
```

Do `header.xml` se v takovém případě v závislosti na konkrétním postavení propíše buď pouze `qUp`, nebo pouze `qDown`, a to do konstanty `q`. V praxi tedy `header.xml` nic neví o tom, že existují nějaká `qUp`a `qDown`, je to čistě interní záležitost cfg_das-u. 

Při kalibraci a auto-kalibraci cfg_dasu, který obsahuje `qUp/qDown`, se příslušná aplikace při startu nejprve zeptá, zda se má kalibrovat pro stoupající nebo klesající postavení, a podle toho upravuje pouze příslušné konstanty.


TODO Popsat další druhy kalibrací.

# List of known signal algorithms

## DomainResampler
    
Indicates that the input signal should be resampled to a different domain

Required inputs: **Source**

## SwapRails

[Synchronous](#time-domain)
    
Copies one of its two inputs depending on the Trolley Orientation to Kilometrage Direction combination. If the Trolley Orientation and the Kilometrage Direction is the same (both positive or both negative), it lets go the **+** input through. If they are in the opposite direction. it lets go the **-** input through. It only honors the Start kilometrage direction, eventual changes in the km direction later during the measurement are not honored.

Required inputs: **+**, **-**

## Chord-Versine3

[Synchronous](#time-domain)

Computes the versine of an asymmetric chord measured by three signals. Specify chord to use using the `Chord` constant.

Required inputs: **InputA, InputB, InputC**

## DropoutFilter

[Asynchronous](#time-domain)

Replaces all outlying points with linear interpolation approximation.
Outliers are defined as clusters of points with value more distant than `MaxDifference`
from the point before the cluster, but containing at most `MaxSamples` points in the cluster.
Constants are read either from Algorithm itself, or from calibration constants prefixed with `DropoutFilter.`.

Required inputs: **Input**

## Gekon-D1

[Asynchronous](#time-domain)

Computes the corrugation signal `D1` from the short chord versine signal and Calibration.D1_Factor. Specify a chord using the `Chord` constant.

Required inputs: **ShortChordVersine**

## Gekon-D2

[Asynchronous](#time-domain)

Computes the corrugation signal `D2` from the short chord versine signal and Calibration.D2_Factor. Specify a chord using the `Chord` constant.

Required inputs: **ShortChordVersine**

## Gekon-D3

[Asynchronous](#time-domain)

Computes the corrugation signal `D3` from the long chord versine signal. Specify a chord using the `Chord` constant.

Required inputs: **LongChordVersine**

## Gekon-D4

[Asynchronous](#time-domain)

Computes the corrugation signal `D4` from the long chord versine signal. Specify a chord using the `Chord` constant.

Required inputs: **LongChordVersine**

## Gekon-D5FromR0

[Asynchronous](#time-domain)

Computes the corrugation signal `D5` from the very long chord versine signal. Specify a chord using the `Chord` constant.

Required inputs: **R0**

## Kilometrage

[Synchronous](#time-domain)

Computes current kilometrage from start kilometrage, direction and track events that alter the kilometrage

Required inputs: **Distance**

## PrimaryGeometryContact-CantSLight

[Synchronous](#time-domain)

Computes Cant for S-Light Krab as `QuasiCant - Top`

Required inputs: **QuasiCant, Top**

## PrimaryGeometryContact-GaugeSLight

[Synchronous](#time-domain)

Computes Gauge for S-Light Krab as `QuasiGauge - Alignment`

Required inputs: **QuasiGauge, Alignment**

## PrimaryGeometryFromProfiles-AlignmentLeft

[Synchronous](#time-domain)

Computes Alignment for the left rail from left profile camera. The formula is `Alignment = HeadSideDefiningPoint.x - CalibrationConstant(AlignmentLeftFromProfilesCorrection)`

Required inputs: **HeadSideDefiningPoint**

## PrimaryGeometryFromProfiles-AlignmentRight

[Synchronous](#time-domain)

Computes Alignment for the right rail from right profile camera. The formula is `Alignment = HeadSideDefiningPoint.x - CalibrationConstant(AlignmentRightFromProfilesCorrection)`

Required inputs: **HeadSideDefiningPoint**

## PrimaryGeometryFromProfiles-Cant

[Synchronous](#time-domain)

Computes Cant from two profile cameras and an inclinometer. The formula is `Cant = HeadTopDefiningPointLeft.z - HeadTopDefiningPointRight.z + QuasiCant`

Required inputs: **QuasiCant, HeadTopDefiningPointLeft, HeadTopDefiningPointRight**

## PrimaryGeometryFromProfiles-GaugeFixedCamera

[Synchronous](#time-domain)

Computes Gauge from two fixed profile cameras. The formula is `Gauge = HeadSideDefiningPointLeft.x + HeadSideDefiningPointRight.x - CalibrationConstant(GaugeFromProfilesCorrection)`

Required inputs: **HeadSideDefiningPointLeft, HeadSideDefiningPointRight**

## PrimaryGeometryFromProfiles-GaugeSlidingCamera

[Synchronous](#time-domain)

Computes Gauge from two profile cameras, one of which is sliding. The formula is `Gauge = HeadSideDefiningPointLeft.x + HeadSideDefiningPointRight.x + QuasiGauge - CalibrationConstant(GaugeFromProfilesCorrection)`

Required inputs: **QuasiGauge, HeadSideDefiningPointLeft, HeadSideDefiningPointRight**

## PrimaryGeometryFromProfiles-TopLeft

[Synchronous](#time-domain)

Computes Top for the left rail from left profile camera. The formula is `Top = HeadTopDefiningPoint.x - CalibrationConstant(TopLeftFromProfilesCorrection)`

Required inputs: **HeadTopDefiningPoint**

## PrimaryGeometryFromProfiles-TopRight

[Synchronous](#time-domain)

Computes Top for the right rail from right profile camera. The formula is `Top = HeadTopDefiningPoint.x - CalibrationConstant(TopRightFromProfilesCorrection)`

Required inputs: **HeadTopDefiningPoint**

## RailProfile-HeadSideDefiningPoint

[Synchronous](#time-domain)

Rail head side point of the processed profile.

Required inputs: **ProcessedProfile**

## RailProfile-HeadTopDefiningPoint

[Synchronous](#time-domain)

Rail head top point of the processed profile.

Required inputs: **ProcessedProfile**

## RailProfile-HorizontalWear

[Synchronous](#time-domain)

Horizontal wear of the processed profile

Required inputs: **ProcessedProfile**

## RailProfile-HorizontalWearPoint

[Synchronous](#time-domain)

Horizontal wear point of the processed profile, that is the point where the wear is measured from

Required inputs: **ProcessedProfile**

## RailProfile-PlasticFlow

[Synchronous](#time-domain)

Plastic flow of the processed profile

Required inputs: **ProcessedProfile**

## RailProfile-PlasticFlowPoint

[Synchronous](#time-domain)

Plastic flow point of the processed profile

Required inputs: **ProcessedProfile**

## RailProfile-Process

[Synchronous](#time-domain)

Computes relevant points and values on the profile
(wear, plastic flow, points classification, head top, side, running edge)
Constants are read either from Algorithm itself, or from calibration constants prefixed with `RailProfile.`.


Required inputs: **CalibratedProfile**

## RailProfile-RailLevel

[Synchronous](#time-domain)

Z coordinate of the detected rail head top point

Required inputs: **ProcessedProfile**

## RailProfile-RunningEdgePoint

[Synchronous](#time-domain)

Running edge point of the processed profile

Required inputs: **ProcessedProfile**

## SoftwareSpeed

[Synchronous](#time-domain)

Computes speed from the travelled distance signal in given domain

Required inputs: **Distance**

## Testing-Copy

[Synchronous](#time-domain)

Just copies input to output, useful for testing purposes

Required inputs: **Input**

## UserCommand-ReleaseKrab

[Synchronous](#time-domain)

On behalf of user interface command performs the Krab emergency release.
That is, it sets the value of the DO signal to 1. The DO signal should be the one selected by `Segment/Setting/ReleaseKrabSignalID` to stop the measurement after one second after 1 has been observed there.

No inputs required.

## UserCommand-SetDigSignal

[Synchronous](#time-domain)

On behalf of user interface commands sets a value of some digital (0/1) signal.

No inputs required.


*Document generated on 2020-05-19T10:29:41.776889498+02:00 by kms 1.2.0-SNAPSHOT*

