KmsConfig {
    application_version: ApplicationVersion {
        application_name: "kms",
        version: "1.2.0-SNAPSHOT",
        scm_info: Some(
            SCMInfo {
                hash: "498281c3d32fcf301209008dcd94ab283be2f183",
                is_dirty: false,
                commit_timestamp: 2020-05-19T10:22:58+02:00,
            },
        ),
    },
    cfg_das_version: 5.0.0,
    device_sn: "123",
    data_dir: "/tmp/kms_data",
    archive_specs: [],
    delete_data_dir_after_archival: true,
    global_algorithm_settings: GlobalAlgorithmSettings {
        constants: CfgDasConstants {
            cfg_das_name: "/home/td/projects/kzv-core/doc/cfg_das_5_0/target/example-Basic VGN signals definition/cfg_das.xml",
            constants_name: "global algorithm constants",
            constants: {
                "TimeDomainBufferedSamples": StringConstantWithUnit {
                    unit: None,
                    name: "TimeDomainBufferedSamples",
                    value: "100",
                },
            },
            phantom_type: PhantomData,
        },
        chords: {},
    },
    domains: Domains {
        domains: [
            Domain {
                domain_name: "GPK",
                sampling_signal_id: "ln",
                sampling_step: 0.25,
            },
        ],
        reference_domain: "GPK",
    },
    important_signals_config: ImportantSignalsConfig {
        distance_signal_id: "ln",
        speed_signal_id: None,
        release_krab_signal_id: None,
        battery_voltage_signal_id: None,
        gps_signals_config: None,
    },
    speed_warning_config: SpeedWarningConfig {
        warn_limit: 8.333333333333334,
        stop_limit: 9.722222222222221,
    },
    battery_config: Some(
        BatteryConfig {
            empty_voltage: 12.0,
            full_voltage: 16.8,
        },
    ),
    calibration_names: "Kalibrace",
    devices_config: DevicesConfig {
        vgn_devices: {
            VGN01: VgnManagerConfig {
                vgn_config: SimulationFunction(
                    SimulationFunctionConfig {
                        simulation_type: Incrementing,
                        frequency: 100.0,
                    },
                ),
                channel_specs: VgnChannelSpecs(Ad: [1, 16], Irc: [Irc 0/Irc]),
                vgn_irc_pause_adjuster_config: VgnIrcPauseAdjusterConfig {
                    pausable_irc_channels: [
                        0,
                    ],
                },
                battery_config: Some(
                    BatteryConfig {
                        empty_voltage: 12.0,
                        full_voltage: 16.8,
                    },
                ),
            },
        },
        gps_devices: {},
        camera_devices: {},
        wecat3d_devices: {},
    },
    pool_part_configs: [
        Source "VGN01 Resampler( --> Channel VGN01:ad:1@GPK(U16), Channel VGN01:ad:16@GPK(U16), Channel VGN01:irc:0@GPK(IrcValue))",
        Source "VGN01 Time Source( --> Channel VGN01:ad:1@Time(U16, max 100 samples), Channel VGN01:ad:16@Time(U16, max 100 samples), Channel VGN01:irc:0@Time(IrcValue, max 100 samples), Signal VGN01@TimeDistance(F64, max 100 samples))",
        Processor "DistanceComputer(Channel VGN01:irc:0@GPK(IrcValue) --> Signal ln@GPK(F64))",
        Processor "DistanceComputer(Channel VGN01:irc:0@Time(IrcValue) --> Signal ln@Time(F64, max 100 samples))",
        Processor "AdCalibrator(Channel VGN01:ad:1@GPK(U16) --> Signal RK@GPK(F64))",
        Processor "AdCalibrator(Channel VGN01:ad:1@Time(U16) --> Signal RK@Time(F64, max 100 samples))",
        Processor "AdCalibrator(Channel VGN01:ad:16@GPK(U16) --> Signal Rych@GPK(F64))",
        Processor "AdCalibrator(Channel VGN01:ad:16@Time(U16) --> Signal Rych@Time(F64, max 100 samples))",
    ],
    digital_outputs_config: DigitalOutputsConfig {
        channels: [],
    },
    savers_config: SaversConfig(channels: {}, signals: {}),
}