## Info 

*Document generated on 2020-05-19T10:29:38.026045584+02:00 by kms 1.2.0-SNAPSHOT*

Version: **5.0.0**

Application: **KMS**

Comment: **Example cfg_das**

Device S/N: 123

# Devices 

| Device | configuration |
| --- | --- |
| VGN01 | SimulationFunction(SimulationFunctionConfig { simulation_type: Incrementing, frequency: 100.0 }) |

# Important Signals

Speed signal: **Not specified**

Krab release signal: **Not specified**. ^[This signal may be changed by the ReleaseKrab command from the user interface. It should be bound to the proper VGN digital output in the Signal configuration with the UserCommand/ReleaseKrab algorithm.When the core detects that this signal has been changed to 1, it will wait for one second and then stop the measurement.]

GPS coordinate signals: Not specified.

Battery voltage signal: **Not specified**

Reference domain: **GPK**

**Domains**

| Domain | Sampling signal | Sampling step [m] |
| --- | --- | --- |
| GPK | ln | 0.25 |

# Algorithm constants

| Constant | Value | 
| --- | --- |
| TimeDomainBufferedSamples | 100 |

# Algorithms

[Overall schema](algorithm-overall.svg)

[Schema for Time domain. ^[Time domain is special, see details in the KMS documentation.]](algorithm-time.svg)

## Domain: **GPK**

[Schema for domain GPK](algorithm-GPK.svg)

| Signal | Name | Source | Save |
| --- | --- | --- | --- |
| **ln** (F64) | Travelled distance | **VGN01:irc:0** (IrcValue) | No |
| **qRK** (F64) | Quasi Gauge | **VGN01:ad:1** (U16) | No |
| **SK** (F64) | Alignment | **VGN01:ad:2** (U16) | No |
| **km** (F64) | Kilometrage | **Kilometrage**<br>*Distance:* ln<br> | No |
| **RK** (F64) | Gauge | **PrimaryGeometryContact-GaugeSLight**<br>*QuasiGauge:* qRK<br>*Alignment:* SK<br> | No |

# Calibrations 


## Calibration **Kalibrace**

[Overall schema for calibration Kalibrace](calibration-Kalibrace-overall.svg)

[Schema for calibration Kalibrace, domain GPK](calibration-Kalibrace-GPK.svg)

IrcOrientation:  Up

KrabVersion: None


### Constants

| Constant | Value | 
| --- | --- |

### Calibration

| Signal | Channel | Calibration |
| --- | --- | --- |
| ??? | VGN01:ad:0 | K: 0, Q: 0 |
| qRK | VGN01:ad:1 | K: 0.01, Q: 10 |
| SK | VGN01:ad:2 | K: 0.02, Q: 20 |
| ??? | VGN01:ad:3 | K: 0.03, Q: 30 |
| ??? | VGN01:ad:16 | K: 0.01, Q: 0 |
| ln | VGN01:irc:0 | K: 0.01, Q: 0 |
