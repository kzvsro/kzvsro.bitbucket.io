KmsConfig {
    application_version: ApplicationVersion {
        application_name: "kms",
        version: "1.2.0-SNAPSHOT",
        scm_info: Some(
            SCMInfo {
                hash: "498281c3d32fcf301209008dcd94ab283be2f183",
                is_dirty: false,
                commit_timestamp: 2020-05-19T10:22:58+02:00,
            },
        ),
    },
    cfg_das_version: 5.0.0,
    device_sn: "123",
    data_dir: "/tmp/kms_data",
    archive_specs: [
        suffix: .kzv, patterns: KrabEvalData.*, Signal_PlasticFlowLeftRail.dat, Signal_PlasticFlowRightRail.dat, Signal_RailProfileLeftRailInside.dat, Signal_RailProfileRightRailInside.dat, events.*, header.*,
    ],
    delete_data_dir_after_archival: true,
    global_algorithm_settings: GlobalAlgorithmSettings {
        constants: CfgDasConstants {
            cfg_das_name: "/home/td/projects/kzv-core/doc/cfg_das_5_0/target/example-SwapRails/cfg_das.xml",
            constants_name: "global algorithm constants",
            constants: {
                "TimeDomainBufferedSamples": StringConstantWithUnit {
                    unit: None,
                    name: "TimeDomainBufferedSamples",
                    value: "100",
                },
            },
            phantom_type: PhantomData,
        },
        chords: {},
    },
    domains: Domains {
        domains: [
            Domain {
                domain_name: "RailProfiles",
                sampling_signal_id: "ln",
                sampling_step: 1.0,
            },
            Domain {
                domain_name: "GPK",
                sampling_signal_id: "ln",
                sampling_step: 0.25,
            },
        ],
        reference_domain: "GPK",
    },
    important_signals_config: ImportantSignalsConfig {
        distance_signal_id: "ln",
        speed_signal_id: None,
        release_krab_signal_id: None,
        battery_voltage_signal_id: None,
        gps_signals_config: None,
    },
    speed_warning_config: SpeedWarningConfig {
        warn_limit: 8.333333333333334,
        stop_limit: 9.722222222222221,
    },
    battery_config: Some(
        BatteryConfig {
            empty_voltage: 12.0,
            full_voltage: 16.8,
        },
    ),
    calibration_names: "Kalibrace",
    devices_config: DevicesConfig {
        vgn_devices: {
            VGN01: VgnManagerConfig {
                vgn_config: SimulationFunction(
                    SimulationFunctionConfig {
                        simulation_type: Incrementing,
                        frequency: 100.0,
                    },
                ),
                channel_specs: VgnChannelSpecs(Ad: [], Irc: [Irc 0/Irc]),
                vgn_irc_pause_adjuster_config: VgnIrcPauseAdjusterConfig {
                    pausable_irc_channels: [
                        0,
                    ],
                },
                battery_config: Some(
                    BatteryConfig {
                        empty_voltage: 12.0,
                        full_voltage: 16.8,
                    },
                ),
            },
        },
        gps_devices: {},
        camera_devices: {},
        wecat3d_devices: {
            WeCat3dLeftInside: SimulationFunctionCorner {
                sampling_frequency: 100.0,
                noise: 0.005,
            },
            WeCat3dRightInside: SimulationFunctionCorner {
                sampling_frequency: 100.0,
                noise: 0.005,
            },
        },
    },
    pool_part_configs: [
        Source "VGN01 Time Source( --> Channel VGN01:irc:0@Time(IrcValue, max 100 samples), Signal VGN01@TimeDistance(F64, max 100 samples))",
        Processor "DistanceComputer(Channel VGN01:irc:0@Time(IrcValue) --> Signal ln@Time(F64, max 100 samples))",
        Source "WeCat3dLeftInside Time Source(Signal ln@Time(F64) --> Channel WeCat3dLeftInside:Points:0@Time(WeCat3dPoints, max 100 samples), Signal WeCat3dLeftInside@TimeDistance(F64, max 100 samples))",
        Source "WeCat3dRightInside Time Source(Signal ln@Time(F64) --> Channel WeCat3dRightInside:Points:0@Time(WeCat3dPoints, max 100 samples), Signal WeCat3dRightInside@TimeDistance(F64, max 100 samples))",
        Processor "RailProfile-Calibrate(Channel WeCat3dLeftInside:Points:0@Time(WeCat3dPoints) --> Signal RailProfileLeftInside@Time(WeCat3dPoints, max 100 samples))",
        Processor "RailProfile-Calibrate(Channel WeCat3dRightInside:Points:0@Time(WeCat3dPoints) --> Signal RailProfileRightInside@Time(WeCat3dPoints, max 100 samples))",
        Processor "RailProfile-Process(Signal RailProfileLeftInside@Time(WeCat3dPoints) --> Signal ProcessedRailProfileLeftInside@Time(ProcessedRailProfile, max 100 samples))",
        Processor "RailProfile-Process(Signal RailProfileRightInside@Time(WeCat3dPoints) --> Signal ProcessedRailProfileRightInside@Time(ProcessedRailProfile, max 100 samples))",
        Source "WeCat3dLeftInside Resampler (Signal ln@Time(F64) --> Channel WeCat3dLeftInside:Points:0@RailProfiles(WeCat3dPoints, max 1000 samples))",
        Source "WeCat3dRightInside Resampler (Signal ln@Time(F64) --> Channel WeCat3dRightInside:Points:0@RailProfiles(WeCat3dPoints, max 1000 samples))",
        Processor "RailProfile-PlasticFlow(Signal ProcessedRailProfileLeftInside@Time(ProcessedRailProfile) --> Signal PlasticFlowLeft@Time(F64, max 100 samples))",
        Processor "RailProfile-PlasticFlow(Signal ProcessedRailProfileRightInside@Time(ProcessedRailProfile) --> Signal PlasticFlowRight@Time(F64, max 100 samples))",
        Source "VGN01 Resampler( --> Channel VGN01:irc:0@GPK(IrcValue))",
        Processor "RailProfile-Calibrate(Channel WeCat3dLeftInside:Points:0@RailProfiles(WeCat3dPoints) --> Signal RailProfileLeftInside@RailProfiles(WeCat3dPoints, max 1000 samples))",
        Processor "RailProfile-Calibrate(Channel WeCat3dRightInside:Points:0@RailProfiles(WeCat3dPoints) --> Signal RailProfileRightInside@RailProfiles(WeCat3dPoints, max 1000 samples))",
        Processor "TimeToDistance Resampler(Average(f64))(Signal WeCat3dLeftInside@TimeDistance(F64), Signal PlasticFlowLeft@Time(F64) --> Signal PlasticFlowLeft@GPK(F64))",
        Processor "TimeToDistance Resampler(Average(f64))(Signal WeCat3dRightInside@TimeDistance(F64), Signal PlasticFlowRight@Time(F64) --> Signal PlasticFlowRight@GPK(F64))",
        Processor "DistanceComputer(Channel VGN01:irc:0@GPK(IrcValue) --> Signal ln@GPK(F64))",
        Processor "RailProfile-Process(Signal RailProfileLeftInside@RailProfiles(WeCat3dPoints) --> Signal ProcessedRailProfileLeftInside@RailProfiles(ProcessedRailProfile, max 1000 samples))",
        Processor "RailProfile-Process(Signal RailProfileRightInside@RailProfiles(WeCat3dPoints) --> Signal ProcessedRailProfileRightInside@RailProfiles(ProcessedRailProfile, max 1000 samples))",
        Processor "Swap rails(Signal RailProfileLeftInside@RailProfiles(WeCat3dPoints), Signal RailProfileRightInside@RailProfiles(WeCat3dPoints) --> Signal RailProfileLeftRailInside@RailProfiles(WeCat3dPoints, max 1000 samples))",
        Processor "Swap rails(Signal PlasticFlowLeft@GPK(F64), Signal PlasticFlowRight@GPK(F64) --> Signal PlasticFlowLeftRail@GPK(F64))",
        Processor "Swap rails(Signal RailProfileRightInside@RailProfiles(WeCat3dPoints), Signal RailProfileLeftInside@RailProfiles(WeCat3dPoints) --> Signal RailProfileRightRailInside@RailProfiles(WeCat3dPoints, max 1000 samples))",
        Processor "Swap rails(Signal PlasticFlowRight@GPK(F64), Signal PlasticFlowLeft@GPK(F64) --> Signal PlasticFlowRightRail@GPK(F64))",
    ],
    digital_outputs_config: DigitalOutputsConfig {
        channels: [],
    },
    savers_config: SaversConfig(channels: {}, signals: {
        Signal RailProfileLeftRailInside@RailProfiles: SignalSaverConfig {
            unit: mm,
            name: "Rail profiles left inside rail",
            saver_precision: Single,
        },
        Signal PlasticFlowLeftRail@GPK: SignalSaverConfig {
            unit: mm,
            name: "Plastic flow left inside rail",
            saver_precision: Single,
        },
        Signal RailProfileRightRailInside@RailProfiles: SignalSaverConfig {
            unit: mm,
            name: "Rail profiles right inside rail",
            saver_precision: Single,
        },
        Signal PlasticFlowRightRail@GPK: SignalSaverConfig {
            unit: mm,
            name: "Plastic flow right inside rail",
            saver_precision: Single,
        },
    }),
}