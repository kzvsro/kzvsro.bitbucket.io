## Info 

*Document generated on 2020-05-19T10:29:40.671838452+02:00 by kms 1.2.0-SNAPSHOT*

Version: **5.0.0**

Application: **KMS**

Comment: **Example cfg_das**

Device S/N: 123

# Devices 

| Device | configuration |
| --- | --- |
| VGN01 | SimulationFunction(SimulationFunctionConfig { simulation_type: Incrementing, frequency: 100.0 }) |
| WeCat3dLeftInside | SimulationFunctionCorner { sampling_frequency: 100.0, noise: 0.005 } |
| WeCat3dRightInside | SimulationFunctionCorner { sampling_frequency: 100.0, noise: 0.005 } |

# Important Signals

Speed signal: **Not specified**

Krab release signal: **Not specified**. ^[This signal may be changed by the ReleaseKrab command from the user interface. It should be bound to the proper VGN digital output in the Signal configuration with the UserCommand/ReleaseKrab algorithm.When the core detects that this signal has been changed to 1, it will wait for one second and then stop the measurement.]

GPS coordinate signals: Not specified.

Battery voltage signal: **Not specified**

Reference domain: **GPK**

**Domains**

| Domain | Sampling signal | Sampling step [m] |
| --- | --- | --- |
| RailProfiles | ln | 1 |
| GPK | ln | 0.25 |

# Algorithm constants

| Constant | Value | 
| --- | --- |
| TimeDomainBufferedSamples | 100 |

# Algorithms

[Overall schema](algorithm-overall.svg)

[Schema for Time domain. ^[Time domain is special, see details in the KMS documentation.]](algorithm-time.svg)

## Domain: **GPK**

[Schema for domain GPK](algorithm-GPK.svg)

| Signal | Name | Source | Save |
| --- | --- | --- | --- |
| **ln** (F64) | Travelled distance | **VGN01:irc:0** (IrcValue) | No |
| **PlasticFlowLeft** (F64) | Plastic flow of rail on the left trolley side | **RailProfile-PlasticFlow**<br>*ProcessedProfile:* ProcessedRailProfileLeftInside@Time<br> | No |
| **PlasticFlowRight** (F64) | Plastic flow of rail on the right trolley side | **RailProfile-PlasticFlow**<br>*ProcessedProfile:* ProcessedRailProfileRightInside@Time<br> | No |
| **PlasticFlowLeftRail** (F64) | Plastic flow left inside rail | **SwapRails**<br>*+:* PlasticFlowLeft<br>*-:* PlasticFlowRight<br> | Signal |
| **PlasticFlowRightRail** (F64) | Plastic flow right inside rail | **SwapRails**<br>*+:* PlasticFlowRight<br>*-:* PlasticFlowLeft<br> | Signal |
## Domain: **RailProfiles**

[Schema for domain RailProfiles](algorithm-RailProfiles.svg)

| Signal | Name | Source | Save |
| --- | --- | --- | --- |
| **RailProfileLeftInside** (WeCat3dPoints) | Rail profiles left inside trolley side | **WeCat3dLeftInside:Points:0** (WeCat3dPoints) | No |
| **ProcessedRailProfileLeftInside** (ProcessedRailProfile) | Processed rail profiles left inside trolley side | **RailProfile-Process**<br>*CalibratedProfile:* RailProfileLeftInside<br> | No |
| **RailProfileRightInside** (WeCat3dPoints) | Rail profiles right inside trolley side | **WeCat3dRightInside:Points:0** (WeCat3dPoints) | No |
| **ProcessedRailProfileRightInside** (ProcessedRailProfile) | Processed rail profiles right inside trolley side | **RailProfile-Process**<br>*CalibratedProfile:* RailProfileRightInside<br> | No |
| **RailProfileLeftRailInside** (WeCat3dPoints) | Rail profiles left inside rail | **SwapRails**<br>*+:* RailProfileLeftInside<br>*-:* RailProfileRightInside<br> | Signal |
| **RailProfileRightRailInside** (WeCat3dPoints) | Rail profiles right inside rail | **SwapRails**<br>*+:* RailProfileRightInside<br>*-:* RailProfileLeftInside<br> | Signal |

# Calibrations 


## Calibration **Kalibrace**

[Overall schema for calibration Kalibrace](calibration-Kalibrace-overall.svg)

[Schema for calibration Kalibrace, domain GPK](calibration-Kalibrace-GPK.svg)

[Schema for calibration Kalibrace, domain RailProfiles](calibration-Kalibrace-RailProfiles.svg)

IrcOrientation:  Up

KrabVersion: None


### Constants

| Constant | Value | 
| --- | --- |
| RailProfile.WearPointToHeadTopVerticalDistance | 14 mm |
| RailProfile.RailBoundingBox | left:-15; right:50; bottom:-150; top:10 mm |
| RailProfile.HeadBoundingBox | left:-15; right:10; bottom:-60; top:0 mm |
| RailProfile.ClusterFilterMinimumPointsInCluster | 5 |
| RailProfile.ClusterFilterMaximumDistance | 1 mm |
| RailProfile.HeadSideDetectorRotationAngle | 0 deg |
| RailProfile.HeadSideDetectorNumberOfPointsInLine | 7 |
| RailProfile.HeadSideDetectorMaximumDeviation | 0.05 mm |
| RailProfile.WearPointXComputerYRange | 0.5 mm |
| RailProfile.WearPointXComputerMaximumXRange | 2 mm |
| RailProfile.PlasticFlowMinimumHorizontalLeapBetweenPlasticFlowAndHeadSide | 1 mm |

### Calibration

| Signal | Channel | Calibration |
| --- | --- | --- |
| ??? | VGN01:ad:0 | K: 0, Q: 0 |
| ??? | VGN01:ad:1 | K: 0.01, Q: 10 |
| ??? | VGN01:ad:2 | K: 0.02, Q: 20 |
| ??? | VGN01:ad:3 | K: 0.03, Q: 30 |
| ??? | VGN01:ad:16 | K: 0.01, Q: 0 |
| ln | VGN01:irc:0 | K: 0.01, Q: 0 |
| RailProfileLeftInside | WeCat3dLeftInside:Points:0 | OffsetX: 0 mm, OffsetZ: 0 mm, Angle: 0°0', Right rail, X Camera axis flipped |
| RailProfileRightInside | WeCat3dRightInside:Points:0 | OffsetX: 0 mm, OffsetZ: 0 mm, Angle: 0°0', Right rail, X Camera axis flipped |
